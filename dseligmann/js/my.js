var Location = Backbone.Model.extend({
});
var Flight = Backbone.Model.extend({
});
var Locations = Backbone.Collection.extend({
    model: Location,
  url: 'http://localhost:8088',
  parse: function(data) {
    return data.GetBA_LocationsResponse.Country;
  },
    
    initialize: function(){
 	console.log("init");   
}
});

var Flights = Backbone.Collection.extend({
    model: Flight,
    url: 'http://localhost:8088',
    parse: function(data) {
        return data.OTA_AirLowFareSearchRS.PricedItineraries.PricedItinerary;
    },

    initialize: function(){
        console.log("init");   
    }
});

var locs = new Locations();
locs.fetch({
    headers: {'target': 'https://api.ba.com/rest-v1/v1/balocations','client-key':'hyr6dqxfxb3kjn3z8zb8d5mm'},
    success: function(coll,resp,opts){
        _.each(coll.models, function(item,index,list){
            //console.log(item);
            var cities = item.get('City');
            if(cities instanceof Array){
                _.each(cities, function (c,c_i,c_l){
                    $('#dep,#arr').each(function(){
                        $(this).append('<li>'+c.CityCode+' - '+c.CityName+' in '+item.get('CountryName')+'</li>');
                    });
                });
            } else {
                var c = item.get('City');
                $('#dep,#arr').each(function(){
                    $(this).append('<li>'+c.CityCode+' - '+c.CityName+' in '+item.get('CountryName')+'</li>');
                });
            }
            });
            $('#dep li').on("click",function(){
                $('#airport1Input').val($(this).text().split(' - ')[0]);
            });
            $('#arr li').on("click",function(){
                $('#airport2Input').val($(this).text().split(' - ')[0]);
            });  
             
        
                    $('#btn-search-flights').on("click",function(){
                        var flights = new Flights();
                        flights.fetch({
                            url: 'https://api.ba.com/rest-v1/v1/flightOfferMktAffiliates;'+
                            'departureDateTimeOutbound='+$('#depDateInput').val()+
                            ';locationCodeOriginOutbound='+$('#airport1Input').val()+
                            ';locationCodeDestinationOutbound='+$('#airport2Input').val()+
                            ';cabin=Economy;ADT=1;CHD=0;INF=0;format=.json',
                            //';cabin=economy;journeyType=roundTrip;range=monthLow',
                            headers: {'Client-Key':'hyr6dqxfxb3kjn3z8zb8d5mm'},
                            success: function(coll,resp,opts){
                                $('#result').empty();
                                console.log(resp);
                                _.each(coll.models,function(p,p_i,p_l){
                                    console.log(p);
                                    var ai = p.get('AirItinerary');
                                    var fs = ai.OriginDestinationOptions.OriginDestinationOption.FlightSegment;
                                    var aipi = p.get('AirItineraryPricingInfo');
                                    var totalFare = aipi.ItinTotalFare.TotalFare['@Amount'] +" "+
                                        aipi.ItinTotalFare.TotalFare['@CurrencyCode'];
                                 	var times = fs['@ArrivalDateTime'].split('T')[1]+" - "+fs['@DepartureDateTime'].split('T')[1];
                                    var flight = fs['@FlightNumber'];
                                    $('#result').append('<li class="list-group-item">'+
                                                        flight+" "+times+" "+totalFare+'</li>');
                                });
                            }
                        });
            }); 
    }
});

